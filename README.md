# 7-minute-workout

Android App that serves as a workout app

Features:
- Timers
- Text to speech
- multiple activities
- BMI calculator activitiy
- Workout history activitiy
- Persistent data using the room databse and persistense best pracrises with the room library (dao-interface etc)
- ...

<table>
    <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/7-minute-workout/-/raw/main/app/src/main/res/drawable/demo_main_screen.png"       width="300" height="585"/>
        </td>
        <td>
            <img src="https://gitlab.com/app-development8/android/7-minute-workout/-/raw/main/app/src/main/res/drawable/demo_exercise_pause.png" width="300" height="585"/>
        </td>
    </tr>
    <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/7-minute-workout/-/raw/main/app/src/main/res/drawable/demo_exercise.png" width="300" height="585"/>
        </td>
                <td>
            <img src="https://gitlab.com/app-development8/android/7-minute-workout/-/raw/main/app/src/main/res/drawable/demo_end.png" width="300" height="585"/>
        </td>
    </tr>
        <tr>
        <td>
            <img src="https://gitlab.com/app-development8/android/7-minute-workout/-/raw/main/app/src/main/res/drawable/demo_bmi.png" width="300" height="585"/>
        </td>
                <td>
            <img src="https://gitlab.com/app-development8/android/7-minute-workout/-/raw/main/app/src/main/res/drawable/demo_history.png" width="300" height="585"/>
        </td>
    </tr>
</table>

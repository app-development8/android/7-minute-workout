package de.peralty.a7_minute_workout.model

import de.peralty.a7_minute_workout.R

object Constants {

    fun defaultExerciseList(): ArrayList<Exercise> {
        val exerciseList = ArrayList<Exercise>()
        val jumpingJacks = Exercise(
            1,
            "Jumping Jacks",
            R.drawable.ic_jumping_jacks,
        )
        val abdominalCrunch = Exercise(
            2,
            "Abdominal Crunch",
            R.drawable.ic_abdominal_crunch,
        )
        val highKneesRunningInPlace = Exercise(
            3,
            "High Knees Running In Place",
            R.drawable.ic_high_knees_running_in_place,
        )
        val lunge = Exercise(
            4,
            "Lunge",
            R.drawable.ic_lunge,
        )
        val plank = Exercise(
            5,
            "Plank",
            R.drawable.ic_plank,
        )
        val pushUp = Exercise(
            6,
            "Push Ups",
            R.drawable.ic_push_up,
        )
        val pushUpAndRotation = Exercise(
            7,
            "Push Up And Rotation",
            R.drawable.ic_push_up_and_rotation,
        )
        val sidePlank = Exercise(
            8,
            "Side Plank",
            R.drawable.ic_side_plank,
        )
        val squat = Exercise(
            9,
            "Squat",
            R.drawable.ic_squat,
        )
        val stepUpOnToChair = Exercise(
            10,
            "Step Up On To Chair",
            R.drawable.ic_step_up_onto_chair,
        )
        val tricepsDipOnChair = Exercise(
            11,
            "Triceps Dip On Chair",
            R.drawable.ic_triceps_dip_on_chair,
        )
        val wallSit = Exercise(
            12,
            "Wall Sit",
            R.drawable.ic_wall_sit,
        )
        exerciseList.add(jumpingJacks)
        exerciseList.add(abdominalCrunch)
        exerciseList.add(highKneesRunningInPlace)
        exerciseList.add(lunge)
        exerciseList.add(plank)
        exerciseList.add(pushUp)
        exerciseList.add(pushUpAndRotation)
        exerciseList.add(sidePlank)
        exerciseList.add(squat)
        exerciseList.add(stepUpOnToChair)
        exerciseList.add(tricepsDipOnChair)
        exerciseList.add(wallSit)
        return exerciseList
    }

}
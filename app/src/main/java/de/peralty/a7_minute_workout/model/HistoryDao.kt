package de.peralty.a7_minute_workout.model

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import kotlinx.coroutines.flow.Flow

@Dao
interface HistoryDao {

    @Insert
    suspend fun insert(history: History)

    @Query("SELECT * FROM history")
    fun fetchAllDates(): Flow<List<History>>

}